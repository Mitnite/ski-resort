import {INavigatorMenu} from "../type";

export const GLOBAL_LOCALIZATION = {

  LOGIN_ERROR_TEXT: 'Введите логин',
  PASSWORD_ERROR_TEXT: 'Введите пароль',
  NAME_ERROR_TEXT: 'Введите имя',
  SURNAME_ERROR_TEXT: 'Введите фамилию',
  FULL_NAME_ERROR_TEXT: 'Введите ФИО',
  BIRTHDATE_ERROR_TEXT: 'Введите дату рождения',
  SKI_PASS_ERROR_TEXT: 'Введите номер ски-пасса',
  SPORT_ERROR_TEXT: 'Укажите вид спорта',
  GENDER_ERROR_TEXT: 'Укажите пол',
  VALIDATOR_LOGIN_ERROR_TEXT: 'Данный логин уже занят',
  VALIDATOR_SKI_PASS_ERROR_TEXT: 'Данный ски-пасс уже зарегистирован',

  FORM_LOGIN_LABEL: 'Логин',
  FORM_PASSWORD_LABEL: 'Пароль',
  FORM_NAME_LABEL: 'Имя',
  FORM_SURNAME_LABEL: 'Фамилия',
  FORM_FULL_NAME_LABEL: 'ФИО',
  FORM_BIRTHDATE_LABEL: 'Дата рождения',
  FORM_GENDER_LABEL: 'Пол',

  FORM_SKI_PASS_LABEL: 'Номер ски-пасса',
  FORM_COACH_LABEL: 'Назначить тренера',
  FORM_VISITOR_LABEL: 'Назначить посетителя',
  FORM_SPORT_LABEL: 'Вид спорта',

  COACH_TITLE: 'Назначенный тренер',

  ROLE_USER: 'User',
  ROLE_ADMIN: 'Administrator',

  EMPTY_VISITORS_TABLE: 'Поситителей ещё не было',
  EMPTY_COACHES_TABLE: 'Инструкторов ещё нет',

}

export const NAVIGATOR_MENU: INavigatorMenu[] = [
  {name: 'Посетители', label: 'visitors', link: '/assets/home/nav-menu/user.svg', hoverLink: '/assets/home/nav-menu/hover/user.svg'},
  {name: 'Инструкторы', label: 'coaches', link: '/assets/home/nav-menu/coach.svg',  hoverLink: '/assets/home/nav-menu/hover/coach.svg'},
  {name: 'Ски-пассы', label: 'pass', link: '/assets/home/nav-menu/pass.svg',  hoverLink: '/assets/home/nav-menu/hover/pass.svg'}
]

export const CARD_ITEM_LOCALIZATION = {
  EDIT: 'Редактировать',
  DELETE: 'Удалить',
  ASSIGN_COACH: 'Назначить тренера',
  ASSIGN_VISITOR: 'Назначить посетителя',

}

export const BUTTON_LABEL_LOCALIZATION = {
  BUTTON_OK_LABEL: 'OK',
  BUTTON_ADD_LABEL: 'Добавить',
  BUTTON_ADD_NEW_LABEL: 'Добавить нового',
  BUTTON_UPDATE_LABEL: 'Обновить',
  BUTTON_ALL_LABEL: 'Все',

}
