import {Component, EventEmitter, OnInit, Output, signal, WritableSignal} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {UsersService} from "../../../services/users.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ISignupForm} from "../../../type";
import {EDIT_USER_POPUP_LOCALIZATION} from "./config/constants";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../config/constant";

@Component({
  selector: 'edit-user-popup',
  templateUrl: './edit-user-popup.component.html',
  styleUrl: './edit-user-popup.component.scss'
})
export class EditUserPopupComponent implements OnInit {

  @Output() closePopup: EventEmitter<void> = new EventEmitter<void>();

  selectedFile: any | null = null;

  isFormValid: WritableSignal<boolean> = signal(true);

  image: string | null =null;

  editUserForm: FormGroup = new FormGroup<ISignupForm>({
    id: new FormControl(null),
    login: new FormControl(null, Validators.required),
    password: new FormControl(null),
    name: new FormControl(null, Validators.required),
    surname: new FormControl(null, Validators.required),
    avatar: new FormControl(null),
    role: new FormControl(null),
  });

  protected readonly EDIT_USER_POPUP_LOCALIZATION = EDIT_USER_POPUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  private readonly USER_ID: string;

  constructor(private cookieService: CookieService, private usersService: UsersService) {
    this.USER_ID = cookieService.get('userLogin');
  }

  ngOnInit(): void {
    const user = this.usersService.getUserById(this.USER_ID);
    if (user) {
      this.editUserForm.patchValue(user);
    }
  }

  onFileSelect(event: any) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsDataURL(event.target.files[0]);
    fileReader.addEventListener('load', () => {
      const url = fileReader.result;
      if (typeof url === "string") {
        this.editUserForm.patchValue({avatar: url});
      }
    })
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.editUserForm.controls[controlName];
    return control.invalid && control.touched;
  }


  onSubmit() {
    const controls = this.editUserForm.controls;

    if (this.editUserForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.usersService.updateUser(this.editUserForm.value)) {
      this.cookieService.set('userLogin', this.editUserForm.value.login);
      this.closePopup.emit();
    } else {
      this.isFormValid.set(false);
    }
  }

}
