export const VISITORS_POPUP_LOCALIZATION = {
  CREATE_TITLE: 'Добавить нового посетителя',
  EDIT_TITLE: 'Редактировать информацию о пользователе',
  INFO_TITLE: 'Карточка посетителя',

  DELETE_TITLE: 'Удаление посетителя',

  DELETE_QUESTION: 'Вы уверены, что хотите удалить карточку этого посетителя?',
}
