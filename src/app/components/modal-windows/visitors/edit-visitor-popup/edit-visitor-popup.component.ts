import {Component, EventEmitter, Input, OnInit, Output, signal, WritableSignal} from '@angular/core';
import {IEditVisitorForm, IVisitors} from "../../../../type";
import {VisitorsService} from "../../../../services/visitors.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {VISITORS_POPUP_LOCALIZATION} from "../config/constants";
import {EDIT_USER_POPUP_LOCALIZATION} from "../../edit-user-popup/config/constants";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../../config/constant";

@Component({
  selector: 'edit-visitor-popup',
  templateUrl: './edit-visitor-popup.component.html',
  styleUrl: './edit-visitor-popup.component.scss'
})
export class EditVisitorPopupComponent implements OnInit{

  @Input() visitor: IVisitors | null = null;
  @Input() isCreateVisitor: boolean = false;

  @Output() closePopup: EventEmitter<boolean> = new EventEmitter<boolean>();

  isFormValid: WritableSignal<boolean> = signal(true);

  editVisitorForm: FormGroup = new FormGroup<IEditVisitorForm>({
    skiPassNumber: new FormControl(null, Validators.required),
    fullName: new FormControl(null, Validators.required),
    birthDate: new FormControl(null, Validators.required),
    coach: new FormControl(null),
    sport: new FormControl(null, Validators.required),
    avatar: new FormControl(null),
  });


  protected readonly VISITORS_POPUP_LOCALIZATION = VISITORS_POPUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private visitorsService: VisitorsService) {
  }

  ngOnInit(): void {
    if (this.visitor) {
      this.editVisitorForm.patchValue(this.visitor);
    }
  }

  onFileSelect(event: any) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsDataURL(event.target.files[0]);
    fileReader.addEventListener('load', () => {
      const url = fileReader.result;
      if (typeof url === "string") {
        this.editVisitorForm.patchValue({avatar: url});
      }
    })
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.editVisitorForm.controls[controlName];
    return control.invalid && control.touched;
  }


  onSubmit() {
    const controls = this.editVisitorForm.controls;

    if (this.editVisitorForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.isCreateVisitor && this.visitorsService.createVisitor(this.editVisitorForm.value)) {
      this.closePopup.emit();
    } else if (!this.isCreateVisitor){
      this.visitorsService.updateVisitor(this.editVisitorForm.value);
      this.closePopup.emit();
    } else {
      this.isFormValid.set(false);
    }

  }


}
