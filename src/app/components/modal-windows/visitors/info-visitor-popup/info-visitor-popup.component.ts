import {Component, EventEmitter, Input, Output, signal, WritableSignal} from '@angular/core';
import {IVisitors} from "../../../../type";
import {VISITORS_POPUP_LOCALIZATION} from "../config/constants";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../../config/constant";

@Component({
  selector: 'info-visitor-popup',
  templateUrl: './info-visitor-popup.component.html',
  styleUrl: './info-visitor-popup.component.scss'
})
export class InfoVisitorPopupComponent {
  @Input() visitor: IVisitors | null = null;
  @Output() closePopup:EventEmitter<void> = new EventEmitter<void>();

  isShowEditVisitorPopup: WritableSignal<boolean> = signal(false);
  isShowDeleteVisitorPopup: WritableSignal<boolean> = signal(false);

  protected readonly VISITORS_POPUP_LOCALIZATION = VISITORS_POPUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  getBirthdate(): string {
    let birthdate: string = '';
    if (this.visitor) {
      birthdate = new Date(this.visitor.birthDate).toLocaleDateString('ru-RU')
    }
    return birthdate
  }

}
