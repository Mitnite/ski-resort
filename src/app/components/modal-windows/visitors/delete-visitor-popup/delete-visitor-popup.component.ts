import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IVisitors} from "../../../../type";
import {VISITORS_POPUP_LOCALIZATION} from "../config/constants";
import {VisitorsService} from "../../../../services/visitors.service";
import {BUTTON_LABEL_LOCALIZATION} from "../../../../config/constant";

@Component({
  selector: 'delete-visitor-popup',
  templateUrl: './delete-visitor-popup.component.html',
})
export class DeleteVisitorPopupComponent {

  @Input() visitor: IVisitors | null = null;

  @Output() closePopup: EventEmitter<void> = new EventEmitter<void>();

  protected readonly VISITORS_POPUP_LOCALIZATION = VISITORS_POPUP_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private visitorsService: VisitorsService) {
  }

  onDeleteVisitor() {
    if (this.visitor){
      this.visitorsService.deleteVisitor(this.visitor);
    }
  }

}
