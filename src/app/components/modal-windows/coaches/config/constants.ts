export const COACHES_POPUP_LOCALIZATION = {
  CREATE_TITLE: 'Добавить нового инструктора',
  EDIT_TITLE: 'Редактировать информацию об инструкторе',
  INFO_TITLE: 'Карточка посетителя',

  DELETE_TITLE: 'Удаление инструктора',

  DELETE_QUESTION: 'Вы уверены, что хотите удалить карточку этого инструктора?',
}
