import {Component, EventEmitter, Input, Output, signal, WritableSignal} from '@angular/core';
import {ICoaches} from "../../../../type";
import {COACHES_POPUP_LOCALIZATION} from "../config/constants";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../../config/constant";

@Component({
  selector: 'info-coach-popup',
  templateUrl: './info-coach-popup.component.html',
  styleUrl: './info-coach-popup.component.scss'
})
export class InfoCoachPopupComponent {
  @Input() coach: ICoaches | null = null;
  @Output() closePopup:EventEmitter<void> = new EventEmitter<void>();

  isShowEditCoachPopup: WritableSignal<boolean> = signal(false);
  isShowDeleteCoachPopup: WritableSignal<boolean> = signal(false);

  protected readonly VISITORS_POPUP_LOCALIZATION = COACHES_POPUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  getBirthdate(): string {
    let birthdate: string = '';
    if (this.coach) {
      birthdate = new Date(this.coach.birthDate).toLocaleDateString('ru-RU')
    }
    return birthdate
  }
}
