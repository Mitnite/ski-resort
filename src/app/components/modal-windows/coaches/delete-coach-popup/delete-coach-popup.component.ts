import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ICoaches} from "../../../../type";
import {COACHES_POPUP_LOCALIZATION} from "../config/constants";
import {BUTTON_LABEL_LOCALIZATION} from "../../../../config/constant";
import {CoachesService} from "../../../../services/coaches.service";

@Component({
  selector: 'delete-coach-popup',
  templateUrl: './delete-coach-popup.component.html',
})
export class DeleteCoachPopupComponent {

  @Input() coach: ICoaches | null = null;

  @Output() closePopup: EventEmitter<void> = new EventEmitter<void>();

  protected readonly VISITORS_POPUP_LOCALIZATION = COACHES_POPUP_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private coachesService: CoachesService) {
  }

  onDeleteVisitor() {
    if (this.coach){
      this.coachesService.deleteCoach(this.coach);
    }
  }

}
