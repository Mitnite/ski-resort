import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ICoaches, IEditCoachForm} from "../../../../type";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {COACHES_POPUP_LOCALIZATION} from "../config/constants";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../../config/constant";
import {CoachesService} from "../../../../services/coaches.service";

@Component({
  selector: 'edit-coach-popup',
  templateUrl: './edit-coach-popup.component.html',
  styleUrl: './edit-coach-popup.component.scss'
})
export class EditCoachPopupComponent implements OnInit {

  @Input() coach: ICoaches | null = null;
  @Input() isCreateCoach: boolean = false;

  @Output() closePopup: EventEmitter<boolean> = new EventEmitter<boolean>();

  editCoachForm: FormGroup = new FormGroup<IEditCoachForm>({
    id: new FormControl(null),
    fullName: new FormControl(null, Validators.required),
    birthDate: new FormControl(null, Validators.required),
    visitor: new FormControl(null),
    gender: new FormControl(null, Validators.required),
    category: new FormControl(null, Validators.required),
    avatar: new FormControl(null),
  });


  protected readonly VISITORS_POPUP_LOCALIZATION = COACHES_POPUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private coachesService: CoachesService) {
  }

  ngOnInit(): void {
    if (this.coach) {
      this.editCoachForm.patchValue(this.coach);
    }
  }

  onFileSelect(event: any) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsDataURL(event.target.files[0]);
    fileReader.addEventListener('load', () => {
      const url = fileReader.result;
      if (typeof url === "string") {
        this.editCoachForm.patchValue({avatar: url});
      }
    })
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.editCoachForm.controls[controlName];
    return control.invalid && control.touched;
  }


  onSubmit() {
    const controls = this.editCoachForm.controls;

    if (this.editCoachForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.isCreateCoach) {
      this.coachesService.createCoach(this.editCoachForm.value);
      this.closePopup.emit();
    } else {
      this.coachesService.updateCoach(this.editCoachForm.value);
      this.closePopup.emit();
    }
  }
}
