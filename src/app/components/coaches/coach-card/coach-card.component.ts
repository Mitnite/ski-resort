import {Component, Input, signal, WritableSignal} from '@angular/core';
import {ICoaches} from "../../../type";
import {CARD_ITEM_LOCALIZATION} from "../../../config/constant";

@Component({
  selector: 'coach-card',
  templateUrl: './coach-card.component.html',
  styleUrl: './coach-card.component.scss'
})
export class CoachCardComponent {
  @Input() coach: ICoaches | null = null;

  isShowMenuItem: WritableSignal<boolean> = signal(false);
  isShowEditCoachPopup: WritableSignal<boolean> = signal(false);
  isShowDeleteCoachPopup: WritableSignal<boolean> = signal(false);
  isShowInfoCoachPopup: WritableSignal<boolean> = signal(false);

  protected readonly CARD_ITEM_LOCALIZATION = CARD_ITEM_LOCALIZATION;

}
