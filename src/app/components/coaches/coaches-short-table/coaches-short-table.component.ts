import {Component, EventEmitter, Input, OnInit, Output, signal, WritableSignal} from '@angular/core';
import {ICoaches} from "../../../type";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../config/constant";
import {CoachesService} from "../../../services/coaches.service";

@Component({
  selector: 'coaches-short-table',
  templateUrl: './coaches-short-table.component.html',
})
export class CoachesShortTableComponent implements OnInit {
  @Input() isShowAllCoaches: boolean = false;
  @Output() showAllVisitors: EventEmitter<void> = new EventEmitter<void>();

  coaches: ICoaches[] = [];
  isShowCreateCoachPopup: WritableSignal<boolean> = signal(false);
  isShowTable: WritableSignal<boolean> = signal(true);

  protected readonly TITLE: string = 'Инструкторы';
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private coachesService: CoachesService) {
  }

  ngOnInit(): void {
    this.coachesService.coachesStream$.subscribe((coaches: ICoaches[]) => {
     this.isShowAllCoaches ?  this.coaches = coaches : this.coaches = coaches.slice(0, 10);
    })
  }
}
