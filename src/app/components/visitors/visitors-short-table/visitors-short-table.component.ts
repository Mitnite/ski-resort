import {Component, EventEmitter, Input, OnInit, Output, signal, WritableSignal} from '@angular/core';
import {VisitorsService} from "../../../services/visitors.service";
import {IVisitors} from "../../../type";
import {BUTTON_LABEL_LOCALIZATION, GLOBAL_LOCALIZATION} from "../../../config/constant";

@Component({
  selector: 'visitors-short-table',
  templateUrl: './visitors-short-table.component.html',
})
export class VisitorsShortTableComponent implements OnInit{

  @Input() isShowAllVisitors: boolean = false;
  @Output() showAllVisitors: EventEmitter<void> = new EventEmitter<void>();

  visitors: IVisitors[] = [];
  isShowCreateVisitorPopup: WritableSignal<boolean> = signal(false);
  isShowTable: WritableSignal<boolean> = signal(true);

  protected readonly TITLE: string = 'Посетители';
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  protected readonly BUTTON_LABEL_LOCALIZATION = BUTTON_LABEL_LOCALIZATION;

  constructor(private visitorsService: VisitorsService) {
  }

  ngOnInit(): void {
    this.visitorsService.visitorStream$.subscribe((visitors: IVisitors[]) => {
      this.isShowAllVisitors ? this.visitors = visitors : this.visitors = visitors.slice(0, 10);
    })
  }
}
