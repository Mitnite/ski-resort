import {Component, Input, signal, WritableSignal} from '@angular/core';
import {IVisitors} from "../../../type";
import {CARD_ITEM_LOCALIZATION} from "../../../config/constant";

@Component({
  selector: 'visitor-card',
  templateUrl: './visitor-card.component.html',
  styleUrl: './visitor-card.component.scss'
})
export class VisitorCardComponent {

  @Input() visitor: IVisitors | null = null;

  protected readonly localDate: Date = new Date();

  isShowMenuItem: WritableSignal<boolean> = signal(false);
  isShowEditVisitorPopup: WritableSignal<boolean> = signal(false);
  isShowDeleteVisitorPopup: WritableSignal<boolean> = signal(false);
  isShowInfoVisitorPopup: WritableSignal<boolean> = signal(false);

  protected readonly CARD_ITEM_LOCALIZATION = CARD_ITEM_LOCALIZATION;

  setLabel(): string {
    if (this.visitor?.birthDate) {
      const visitorAge: Date = new Date(this.visitor.birthDate)
      const age: number = visitorAge.getFullYear() - this.localDate.getFullYear()
      switch (-age % 10) {
        case (1):
          return `${-age} год`
        case (2):
          return `${-age} годa`
        case (3):
          return `${-age} годa`
        case (4):
          return `${-age} годa`
        default :
          return `${-age} лет`
      }
    } else {
      return ' '
    }
  }

}
