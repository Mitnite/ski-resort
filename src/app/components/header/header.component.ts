import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {HEADER_LOCALIZATION} from "./config/constants";

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {

  protected readonly HEADER_LOCALIZATION = HEADER_LOCALIZATION;

  constructor(private CookieService: CookieService, private router: Router) {
  }

  exitButton() {
    this.CookieService.delete('userLogin')
    this.router.navigate(['/login'])
  }

}
