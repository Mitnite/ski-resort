import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {HomeComponent} from "./pages/home/home.component";
import {SigninComponent} from "./pages/signin/signin.component";
import {AppRoutingModule} from "./app-routing.module";
import {CookieService} from "ngx-cookie-service";
import {NgOptimizedImage} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SignupComponent} from "./pages/signup/signup.component";
import {RouterLink} from "@angular/router";
import {HeaderComponent} from "./components/header/header.component";
import {BackdropComponent} from "./components/backdrop/backdrop.component";
import {EditUserPopupComponent} from "./components/modal-windows/edit-user-popup/edit-user-popup.component";
import {VisitorCardComponent} from "./components/visitors/visitor-card/visitor-card.component";
import {VisitorsShortTableComponent} from "./components/visitors/visitors-short-table/visitors-short-table.component";
import {
  EditVisitorPopupComponent
} from "./components/modal-windows/visitors/edit-visitor-popup/edit-visitor-popup.component";
import {
  DeleteVisitorPopupComponent
} from "./components/modal-windows/visitors/delete-visitor-popup/delete-visitor-popup.component";
import {
  InfoVisitorPopupComponent
} from "./components/modal-windows/visitors/info-visitor-popup/info-visitor-popup.component";
import {CoachesShortTableComponent} from "./components/coaches/coaches-short-table/coaches-short-table.component";
import {CoachCardComponent} from "./components/coaches/coach-card/coach-card.component";
import {EditCoachPopupComponent} from "./components/modal-windows/coaches/edit-coach-popup/edit-coach-popup.component";
import {
  DeleteCoachPopupComponent
} from "./components/modal-windows/coaches/delete-coach-popup/delete-coach-popup.component";
import {InfoCoachPopupComponent} from "./components/modal-windows/coaches/info-coach-popup/info-coach-popup.component";

@NgModule({
  declarations: [
    HomeComponent,
    AppComponent,
    SigninComponent,
    SignupComponent,
    HeaderComponent,
    BackdropComponent,
    EditUserPopupComponent,
    VisitorCardComponent,
    VisitorsShortTableComponent,
    EditVisitorPopupComponent,
    DeleteVisitorPopupComponent,
    InfoVisitorPopupComponent,
    CoachCardComponent,
    CoachesShortTableComponent,
    EditCoachPopupComponent,
    DeleteCoachPopupComponent,
    InfoCoachPopupComponent


  ],
  imports: [
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgOptimizedImage,
    RouterLink,
    FormsModule
  ],
  providers: [CookieService],
  exports: [
    EditCoachPopupComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
