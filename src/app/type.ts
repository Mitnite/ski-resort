import {FormControl} from "@angular/forms";

export interface IUser {
  id: string;
  login: string;
  password: string;
  name: string;
  surname: string;
  role: string;
  avatar: string;
}

export interface IVisitors {
  skiPassNumber: string;
  fullName: string;
  birthDate: Date;
  coach: IAssign | null;
  sport: string;
  avatar: string | null;
}

export interface ICoaches {
  id: number;
  fullName: string;
  birthDate: Date;
  gender: string;
  visitor: IAssign | null;
  category: string;
  avatar: string | null;
}

interface IAssign {
  id: number
  fullName: string
}


export interface ISigninForm {
  login: FormControl<string | null>;
  password: FormControl<string | null>;
}

export interface ISignupForm {
  id: FormControl<string | null>;
  login: FormControl<string | null>;
  password: FormControl<string | null>;

  name: FormControl<string | null>;
  surname: FormControl<string | null>;

  avatar: FormControl<string | null>;
  role: FormControl<string | null>;
}

export interface INavigatorMenu {
  name: string;
  link: string;
  label: string;
  hoverLink: string;
}

export interface IEditVisitorForm {
  skiPassNumber: FormControl<string | null>;
  fullName: FormControl<string | null>;
  birthDate: FormControl<Date | null>;
  coach: FormControl<IAssign | null>;
  sport: FormControl<string | null>;
  avatar: FormControl<string | null>;
}

export interface IEditCoachForm {
  id: FormControl<number | null>;
  fullName: FormControl<string | null>;
  birthDate: FormControl<Date | null>;
  gender: FormControl<string | null>;
  visitor: FormControl<IAssign | null>;
  category: FormControl<string | null>;
  avatar: FormControl<string | null>;
}
