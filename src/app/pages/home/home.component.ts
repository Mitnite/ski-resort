import {Component, OnInit, signal, WritableSignal} from '@angular/core';
import {UsersService} from "../../services/users.service";
import {CookieService} from "ngx-cookie-service";
import {INavigatorMenu, IUser} from "../../type";
import {HOME_LOCALIZATION} from "./config/constants";
import {NAVIGATOR_MENU} from "../../config/constant";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit{

  user: IUser | null = null;

  selectPage: WritableSignal<string> = signal('all')

  isShowUserInfo: WritableSignal<boolean> = signal(false);

  private readonly USER_ID: string;

  protected readonly HOME_LOCALIZATION = HOME_LOCALIZATION;
  protected readonly NAVIGATOR_MENU: INavigatorMenu[] = NAVIGATOR_MENU;

  constructor(private usersService: UsersService, cookieService: CookieService) {
        this.USER_ID = cookieService.get('userLogin')
  }

  ngOnInit(): void {
    this.user = this.usersService.getUserById(this.USER_ID)
  }



}
