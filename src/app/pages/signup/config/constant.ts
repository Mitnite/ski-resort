export const SIGNUP_LOCALIZATION = {
  ACCOUNT: 'личный кабинет',
  RESORT: 'Горнолыжного курорта',
  COPYRIGHT: '(с) 2021. Все права защищены',
  BACK: 'Назад',
  SIGNUP_BUTTON_LABEL: 'Зарегистироваться',

}
