import {Component, signal, WritableSignal} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ISignupForm} from "../../type";
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {CookieService} from "ngx-cookie-service";
import { SIGNUP_LOCALIZATION} from "./config/constant";
import {GLOBAL_LOCALIZATION} from "../../config/constant";

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.scss'
})

export class SignupComponent {

  isDataValid: WritableSignal<boolean> = signal(true);

  private readonly ROLE: string = GLOBAL_LOCALIZATION.ROLE_USER;

  signupForm: FormGroup = new FormGroup<ISignupForm>({
    id: new FormControl(null),
    login: new FormControl(null, [Validators.required]),
    password: new FormControl(null, Validators.required),
    name: new FormControl(null, Validators.required),
    surname: new FormControl(null, Validators.required),
    avatar: new FormControl(null),
    role: new FormControl(this.ROLE),
  });

  protected readonly SIGNUP_LOCALIZATION = SIGNUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;
  constructor(private router: Router, private cookieService: CookieService, private usersService: UsersService) {
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.signupForm.controls[controlName];
    return control.invalid && control.touched;
  }

  onSubmit() {
    const controls = this.signupForm.controls;
    this.signupForm.patchValue({id: this.signupForm.value.login})
    if (this.signupForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.usersService.signUp(this.signupForm.value)) {
      this.cookieService.set('userLogin', this.signupForm.value.login);
      this.router.navigate(['/']);
    } else {
      this.isDataValid.set(false);
    }
  }

}
