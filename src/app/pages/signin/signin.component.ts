import {Component, signal, Signal, WritableSignal} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ISigninForm} from "../../type";
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {CookieService} from "ngx-cookie-service";
import {SIGNIN_LOCALIZATION} from "./config/constant";
import {GLOBAL_LOCALIZATION} from "../../config/constant";

@Component({
  selector: 'signin',
  templateUrl: './signin.component.html',
  styleUrl: './signin.component.scss'
})

export class SigninComponent {

  isDataValid: WritableSignal<boolean> = signal(true);

  signInForm: FormGroup = new FormGroup<ISigninForm>({
    login: new FormControl(null, [Validators.required]),
    password: new FormControl(null, Validators.required,),
  });

  protected readonly SIGNIN_LOCALIZATION = SIGNIN_LOCALIZATION
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION

  constructor(private router: Router, private cookieService: CookieService, private usersService: UsersService) {
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.signInForm.controls[controlName];
    return control.invalid && control.touched;
  }

  onSubmit() {
    const controls = this.signInForm.controls;

    if (this.signInForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.usersService.signIn(this.signInForm.value)) {
      this.cookieService.set('userLogin', this.signInForm.value.login);
      this.router.navigate(['/']);
    } else {
      this.isDataValid.set(false);
    }
  }
}
