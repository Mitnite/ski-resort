import {Injectable} from "@angular/core";
import {IVisitors} from "../type";
import {BehaviorSubject} from "rxjs";
import {StorageService} from "./localStorage.service";

@Injectable({
  providedIn: 'root'
})


export class VisitorsService {
  private name: string = 'Visitors';
  private readonly visitors: IVisitors[] = []
  visitorStream$: BehaviorSubject<IVisitors[]>;

  constructor(private StorageService: StorageService) {
    const storage: string | null = this.StorageService.getItem(this.name);
    if (storage) {
      this.visitors = JSON.parse(storage);
    }
    this.visitorStream$ = new BehaviorSubject(this.visitors);
  }

  createVisitor(newVisitor: IVisitors): boolean {
    const accept = this.visitors.find((visitor: IVisitors) => visitor.skiPassNumber === newVisitor.skiPassNumber)
    if (!!accept) {
      return false;
    } else {
      this.visitors.push(newVisitor);
      this.saveLocalStorage();
      return true;
    }
  }

  updateVisitor(editedVisitor: IVisitors) {
    this.visitors.forEach((VisitorItem: IVisitors) => {
      if (VisitorItem.skiPassNumber === editedVisitor.skiPassNumber) {
        Object.assign(VisitorItem, editedVisitor)
      }
    });
    this.saveLocalStorage();
  }

  deleteVisitor(deletedVisitor: IVisitors) {
    this.visitors.forEach((VisitorItem: IVisitors, index: number) => {
      if (VisitorItem.skiPassNumber === deletedVisitor.skiPassNumber) {
        this.visitors.splice(index, 1);
      }
    });
    this.saveLocalStorage();
  }

  protected saveLocalStorage() {
    this.visitorStream$.next(this.visitors);
    this.StorageService.setItem(this.name, this.visitors);
  }
}
