import {IUser} from "../type";
import {Injectable} from '@angular/core';
import {StorageService} from "./localStorage.service";

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  private name: string = 'Users';
  private readonly users: IUser[] = [];

  constructor(private StorageService: StorageService) {
    const storage: string | null = this.StorageService.getItem(this.name);
    if (storage) {
      this.users = JSON.parse(storage);
    }
  }

  signUp(item: IUser): boolean {
    const existingUser: IUser | undefined = this.users.find((user) => user.login === item.login);

    if (existingUser) {
      return false;
    } else {
      item.id = item.login;
      this.users.push(item);
      this.StorageService.setItem(this.name, this.users);
      return true;
    }
  }

  signIn(item: IUser): boolean {
    const accept: IUser | undefined = this.users.find((user) => (user.login === item.login) && (user.password === item.password));
    return !!accept;
  }

  updateUser(editedUser: IUser): boolean {
    const accept: IUser | undefined = this.users.find((user) => (user.login === editedUser.login) && (user.id !== editedUser.id));
    if (!accept) {
      this.users.forEach((user: IUser) => {
        if (user.id === editedUser.id) {
          editedUser.id = editedUser.login;
          Object.assign(user, editedUser);
        }
      })
      this.StorageService.setItem(this.name, this.users);
      return true
    } else {
      return false
    }
  }

  getUserById(id: string): IUser | null {
    return this.users.find((user: IUser) => (user.login === id)) || null
  }
}
