import {Injectable} from '@angular/core';
import {ICoaches, IUser, IVisitors} from "../type";

@Injectable({providedIn: 'root'})

export class StorageService {
  setItem(key: string, value: IUser[] | IVisitors[] | ICoaches[]) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getItem(key: string): any {
    return localStorage.getItem(key);
  }

  removeItem(key: string) {
    localStorage.removeItem(key);
  }

  clear(): void {
    localStorage.clear();
  }
}
