import {ICoaches} from "../type";
import {BehaviorSubject} from "rxjs";
import {StorageService} from "./localStorage.service";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class CoachesService {

  private name: string = 'Coaches';
  private readonly coaches: ICoaches[] = [];
  coachesStream$:  BehaviorSubject<ICoaches[]>;

  constructor(private StorageService: StorageService) {
    const storage: string | null = this.StorageService.getItem(this.name);
    if (storage) {
      this.coaches = JSON.parse(storage);
    }
    this.coachesStream$ = new BehaviorSubject(this.coaches);
  }

  createCoach(newVisitor: ICoaches) {
      newVisitor.id = this.coaches.length > 0
        ? this.coaches[this.coaches.length - 1].id + 1
        : 11000;
      this.coaches.push(newVisitor);
      this.saveLocalStorage();

  }

  updateCoach(editedVisitor: ICoaches) {
    this.coaches.forEach((VisitorItem: ICoaches) => {
      if (VisitorItem.id === editedVisitor.id) {
        Object.assign(VisitorItem, editedVisitor)
      }
    });
    this.saveLocalStorage();
  }

  deleteCoach(deletedVisitor: ICoaches) {
    this.coaches.forEach((VisitorItem: ICoaches, index: number) => {
      if (VisitorItem.id === deletedVisitor.id) {
        this.coaches.splice(index, 1);
      }
    });
    this.saveLocalStorage();
  }

  protected saveLocalStorage() {
    this.coachesStream$.next(this.coaches);
    this.StorageService.setItem(this.name, this.coaches);
  }

}
