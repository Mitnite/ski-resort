import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {SigninComponent} from "./pages/signin/signin.component";
import {IsAuthorizationGuard} from "./guards/authorization.guard";
import {SignupComponent} from "./pages/signup/signup.component";


const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [IsAuthorizationGuard]},
  {path: 'login', component: SigninComponent},
  {path: 'registration', component: SignupComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
